package hr.dturic.faculty.dinamikoprogramiranje.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import hr.dturic.faculty.dinamikoprogramiranje.R
import hr.dturic.faculty.dinamikoprogramiranje.fragment.InputFragment
import hr.dturic.faculty.dinamikoprogramiranje.fragment.OutputFragment

class MainActivity : AppCompatActivity(), InputFragment.OnFragmentInteractionListener {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        replaceFragment(InputFragment.newInstance())

        //oneStep.text = mIteration.calculateStep(2, 0f, 30f, DynamicProgramingLogic.caclculateQuantity(0f, 30f))

        //text.text = matrixHolder.printMatrix()
    }

    fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.also {
            it.beginTransaction()
                    .replace(android.R.id.content, fragment)
                    .commit()
        }
    }

    override fun onDateEntered() {

    }
}
