package hr.dturic.faculty.dinamikoprogramiranje.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import hr.dturic.faculty.dinamikoprogramiranje.R
import hr.dturic.faculty.dinamikoprogramiranje.activity.ResultActivity
import kotlinx.android.synthetic.main.fragment_input.*

class InputFragment : Fragment() {

    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_input, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        periodCountEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s.isNullOrBlank())
                    prepareRates(0)
                else if (s.toString().toInt() < 0)
                    prepareRates(0)
                else
                    prepareRates(s.toString().toInt())
            }

        })

        calculate.setOnClickListener {
            prepareData()
            listener?.onDateEntered()
        }
    }

    private fun prepareData() {

        val rates = ratesEditText.text.toString().toFloat()
        val maxOrder = maxOrderEditText.text.toString().toFloat()
        val orderCost = orderCostEditText.text.toString().toFloat()
        val maxWarehouse = maxWarehouseEditText.text.toString().toFloat()
        val warehouseCost = warehouseCostEditText.text.toString().toFloat()
        val periodCount = periodCountEditText.text.toString().toInt()

        val periods = getRates()

        val intent = Intent(requireContext(), ResultActivity::class.java)

        intent.putExtra(RATES, rates)
        intent.putExtra(MAX_ORDER, maxOrder)
        intent.putExtra(ORDER_COST, orderCost)
        intent.putExtra(MAX_WAREHOUSE, maxWarehouse)
        intent.putExtra(WAREHOUSE_COST, warehouseCost)
        intent.putExtra(PERIOD_COUNT, periodCount)
        intent.putExtra(PERIODS, periods)

        startActivity(intent)

    }

    @SuppressLint("SetTextI18n")
    private fun prepareRates(count: Int) {

        (periodHolder as ViewGroup).removeAllViews()

        for (i in 0 until count) {
            val view =
                    LayoutInflater.from(requireContext()).inflate(R.layout.period_item_view, view as ViewGroup, false)

            view.findViewById<TextView>(R.id.label).text = "Period ${i + 1}"

            periodHolder.addView(view)
        }
    }

    private fun getRates(): FloatArray {

        val result = FloatArray(periodHolder.childCount)

        for (i in 0 until periodHolder.childCount) {
            result[i] = periodHolder.getChildAt(i).findViewById<EditText>(R.id.input).text.toString().toFloat()
        }

        return result
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onDateEntered()
    }

    companion object {

        const val RATES = "rates"
        const val MAX_ORDER = "max_order"
        const val ORDER_COST = "order_cost"
        const val MAX_WAREHOUSE = "max_warehouse"
        const val WAREHOUSE_COST = "warehouseCost"
        const val PERIOD_COUNT = "period_count"
        const val PERIODS = "periods"

        @JvmStatic
        fun newInstance() = InputFragment()
    }
}
