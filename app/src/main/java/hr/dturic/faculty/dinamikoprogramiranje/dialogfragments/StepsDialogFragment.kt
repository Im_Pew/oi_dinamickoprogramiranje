package hr.dturic.faculty.dinamikoprogramiranje.dialogfragments

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hr.dturic.faculty.dinamikoprogramiranje.R
import kotlinx.android.synthetic.main.dialog_fragment_steps.*

class StepsDialogFragment : DialogFragment() {

    internal var titleText: String? = null
        set(value) {
            field = value
        }
    internal var contentText: String? = null
        set(value) {
            field = value
        }
    internal var buttonText: String? = null
        set(value) {
            field = value
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        return inflater.inflate(R.layout.dialog_fragment_steps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (titleText != null)
            title.text = this.titleText

        if (contentText != null)
            content.text = contentText

        if (buttonText != null)
            dismiss.text = buttonText

        dismiss.setOnClickListener {
            dismiss()
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() = StepsDialogFragment()

    }
}

class DialogFragmentBuilder {

    private lateinit var builder: StepsDialogFragment

    fun Builder(): DialogFragmentBuilder {
        builder = StepsDialogFragment()
        return this
    }

    fun setTitleText(titleText: String): DialogFragmentBuilder {
        builder.titleText = titleText
        return this
    }

    fun setContentText(contentText: String): DialogFragmentBuilder {
        builder.contentText = contentText
        return this
    }

    fun setButtonText(buttonText: String): DialogFragmentBuilder {
        builder.buttonText = buttonText
        return this
    }

    fun build(fm: FragmentManager): StepsDialogFragment {
        val fragment = StepsDialogFragment()

        fragment.titleText = builder.titleText
        fragment.contentText = builder.contentText
        fragment.buttonText = builder.buttonText

        fragment.show(fm, "")

        return fragment
    }
}