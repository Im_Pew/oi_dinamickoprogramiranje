package hr.dturic.faculty.dinamikoprogramiranje.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import hr.dturic.faculty.dinamikoprogramiranje.R
import hr.dturic.faculty.dinamikoprogramiranje.common.MatrixGetter
import hr.dturic.faculty.dinamikoprogramiranje.fragment.InputFragment
import hr.dturic.faculty.dinamikoprogramiranje.fragment.OutputFragment
import hr.dturic.faculty.dinamikoprogramiranje.fragment.TableFragment
import hr.dturic.faculty.dinamikoprogramiranje.matrix.DynamicProgramingLogic
import hr.dturic.faculty.dinamikoprogramiranje.matrix.Iteration

class ResultActivity : AppCompatActivity(), MatrixGetter {

    private var mRate = 0f
    private lateinit var mPeriods: FloatArray
    private var mPeriodCount: Int = 0
    private var mWarehouseCost: Float = 0.0f
    private var mOrderCost: Float = 0.0f

    private lateinit var matrixHolder: DynamicProgramingLogic

    private lateinit var mIteration: Iteration


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        getIntentData(intent)
    }

    private fun calculateMatrix() {

        val rates = DynamicProgramingLogic.caclculateQuantity(DynamicProgramingLogic.mWarehouseCap, mPeriods[0])
        val initialQuantity = FloatArray(rates.count())


        for (i in 0 until rates.count()) {
            initialQuantity[i] = mRate * i
        }

        matrixHolder = DynamicProgramingLogic(rates.count(), (mPeriodCount * 2) + 1, initialQuantity)


        for (i in 0 until rates.count())
            matrixHolder.insertValue(i, 1, rates[i])

        mIteration = Iteration(mRate, DynamicProgramingLogic.mMaxOrder, mOrderCost, mWarehouseCost, matrixHolder)


        for (j in 2..mPeriodCount)
            for (i in 0 until matrixHolder.mRows)
                mIteration.calculateStepValue(j, 0f, mPeriods[j - 1])

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)

        menuInflater.inflate(R.menu.result_menu, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        super.onOptionsItemSelected(item)

        when (item?.itemId) {
            R.id.matrix_menu_button -> supportFragmentManager.apply {
                beginTransaction()
                    .replace(android.R.id.content, OutputFragment.newInstance(this@ResultActivity))
                    .commit()
            }
            R.id.table_menu_button -> supportFragmentManager.apply {
                beginTransaction()
                    .replace(android.R.id.content, TableFragment.newInstance(this@ResultActivity))
                    .commit()
            }
        }

        return false
    }

    override fun getMatrix(): DynamicProgramingLogic {
        return matrixHolder
    }

    override fun getIteration(): Iteration {
        return mIteration
    }

    override fun getPeriods(): FloatArray {
        return mPeriods
    }

    private fun getIntentData(intent: Intent) {

        mRate = intent.getFloatExtra(InputFragment.RATES, 0f)
        DynamicProgramingLogic.mMaxOrder = intent.getFloatExtra(InputFragment.MAX_ORDER, 0f)
        mOrderCost = intent.getFloatExtra(InputFragment.ORDER_COST, 0f)
        DynamicProgramingLogic.mWarehouseCap = intent.getFloatExtra(InputFragment.MAX_WAREHOUSE, 0f)
        mWarehouseCost = intent.getFloatExtra(InputFragment.WAREHOUSE_COST, 0f)
        mPeriodCount = intent.getIntExtra(InputFragment.PERIOD_COUNT, 0)

        DynamicProgramingLogic.mRates = mRate

        mPeriods = intent.getFloatArrayExtra(InputFragment.PERIODS)

        calculateMatrix()

        supportFragmentManager.beginTransaction()
            .replace(android.R.id.content, OutputFragment.newInstance(this))
            .commit()
    }
}
