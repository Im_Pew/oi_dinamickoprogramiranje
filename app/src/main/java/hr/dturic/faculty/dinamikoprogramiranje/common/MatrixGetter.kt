package hr.dturic.faculty.dinamikoprogramiranje.common

import hr.dturic.faculty.dinamikoprogramiranje.matrix.DynamicProgramingLogic
import hr.dturic.faculty.dinamikoprogramiranje.matrix.Iteration

interface MatrixGetter {

    fun getMatrix(): DynamicProgramingLogic

    fun getIteration(): Iteration

    fun getPeriods(): FloatArray
}