package hr.dturic.faculty.dinamikoprogramiranje.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import hr.dturic.faculty.dinamikoprogramiranje.R
import hr.dturic.faculty.dinamikoprogramiranje.common.MatrixGetter
import hr.dturic.faculty.dinamikoprogramiranje.matrix.DynamicProgramingLogic
import hr.dturic.faculty.dinamikoprogramiranje.matrix.Iteration
import kotlinx.android.synthetic.main.fragment_table.*


class TableFragment : Fragment() {

    private var table: Array<FloatArray>? = null

    private var getter: MatrixGetter? = null

    private var matrixHolder: DynamicProgramingLogic? = null
    private var mIteration: Iteration? = null
    private var mPeriods: FloatArray? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        matrixHolder = getter?.getMatrix()
        mIteration = getter?.getIteration()
        mPeriods = getter?.getPeriods()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_table, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initTable()

        calculateTable()

        writeMatrix(table!!)
        calculateSum()
        calculateQuantity()

        var print = ""
        for (row in table!!) {
            for (cell in row)
                print += cell.toString()
            print += "\n"
        }
    }

    fun setMatrixGetter(listener: MatrixGetter) {
        getter = listener
    }

    private fun initTable() {
        val rows = mPeriods?.size

        table = Array(size = (rows!!), init = { FloatArray(size = 5) })
    }

    private fun calculateTable() {

        for ((i, period) in mPeriods!!.withIndex()) {
            table!![i][0] = (i + 1).toFloat()
            table!![i][3] = period
        }

        table!![0][1] = 0f
        table!![mPeriods!!.size - 1][4] = 0f

        for (i in mPeriods!!.size - 1 downTo 0) {
            table!![i][2] = matrixHolder!!.getValueOfQuantity(table!![i][4], i)

            table!![i][1] = table!![i][4] + table!![i][3] - table!![i][2]

            if (i > 0) {
                table!![i - 1][4] = table!![i][1]
            }
        }
    }

    private fun calculateSum() {

        val viewHolder = LinearLayout(requireContext())
        viewHolder.orientation = LinearLayout.VERTICAL

        var value: Float
        var sum = 0f

        for (i in 0 until mPeriods!!.size) {
            if (table!![i][2] != 0f) {
                value = 700f
            } else {
                value = 0f
            }
            sum += value

            val cell = LayoutInflater.from(requireContext()).inflate(R.layout.cell, view as ViewGroup, false)

            cell.findViewById<TextView>(R.id.cell_label).text = value.toString()

            viewHolder.addView(cell)
        }

        val cell = LayoutInflater.from(requireContext()).inflate(R.layout.cell, view as ViewGroup, false)

        cell.findViewById<TextView>(R.id.cell_label).text = sum.toString()

        viewHolder.addView(cell)

        table_holder.addView(viewHolder)
    }

    private fun calculateQuantity() {

        val viewHolder = LinearLayout(requireContext())
        viewHolder.orientation = LinearLayout.VERTICAL

        var sum = 0f
        var value: Float

        for (i in 0 until mPeriods!!.size) {
            value = table!![i][4] * mIteration!!.mStorageCost
            sum += value

            val cell = LayoutInflater.from(requireContext()).inflate(R.layout.cell, view as ViewGroup, false)

            cell.findViewById<TextView>(R.id.cell_label).text = value.toString()

            viewHolder.addView(cell)
        }

        val cell = LayoutInflater.from(requireContext()).inflate(R.layout.cell, view as ViewGroup, false)

        cell.findViewById<TextView>(R.id.cell_label).text = sum.toString()

        viewHolder.addView(cell)

        table_holder.addView(viewHolder)

    }

    private fun writeMatrix(matrix: Array<FloatArray>) {

        for (i in 0 until matrix[0].size) {

            val viewHolder = LinearLayout(requireContext())
            viewHolder.orientation = LinearLayout.VERTICAL

            for (j in 0 until matrix.size) {
                val cell = LayoutInflater.from(requireContext()).inflate(R.layout.cell, view as ViewGroup, false)

                cell.findViewById<TextView>(R.id.cell_label).text = matrix[j][i].toString()

                viewHolder.addView(cell)
            }

            table_holder.addView(viewHolder)
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            TableFragment()


        @JvmStatic
        fun newInstance(listener: MatrixGetter): TableFragment {
            val fragment = TableFragment()

            fragment.setMatrixGetter(listener)

            return fragment

        }
    }
}
