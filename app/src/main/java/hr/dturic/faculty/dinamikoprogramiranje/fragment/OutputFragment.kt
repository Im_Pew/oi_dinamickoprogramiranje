package hr.dturic.faculty.dinamikoprogramiranje.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import hr.dturic.faculty.dinamikoprogramiranje.R
import hr.dturic.faculty.dinamikoprogramiranje.common.MatrixGetter
import hr.dturic.faculty.dinamikoprogramiranje.dialogfragments.DialogFragmentBuilder
import hr.dturic.faculty.dinamikoprogramiranje.matrix.DynamicProgramingLogic
import hr.dturic.faculty.dinamikoprogramiranje.matrix.Iteration
import kotlinx.android.synthetic.main.fragment_output.*
import kotlin.Int.Companion as Int1

class OutputFragment : Fragment() {

    private var getter: MatrixGetter? = null

    private var matrixHolder: DynamicProgramingLogic? = null
    private var mIteration: Iteration? = null
    private var mPeriods: FloatArray? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        matrixHolder = getter?.getMatrix()
        mIteration = getter?.getIteration()
        mPeriods = getter?.getPeriods()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_output, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        writeMatrix(matrixHolder?.matrix!!)
    }

    public fun setMatrixGetter(listener: MatrixGetter) {
        getter = listener
    }

    @SuppressLint("SetTextI18n")
    private fun writeMatrix(matrix: Array<FloatArray>) {

        for (i in 0 until matrixHolder?.mColumns!!) {

            val viewHolder = LinearLayout(requireContext())
            viewHolder.orientation = LinearLayout.VERTICAL

            if (i == 0) {
                val cell = LayoutInflater.from(requireContext()).inflate(R.layout.cell, view as ViewGroup, false)

                cell.findViewById<TextView>(R.id.cell_label).text = "  I(i)  "

                viewHolder.addView(cell)
            } else if (i % 2 == 1) {
                val cell = LayoutInflater.from(requireContext()).inflate(R.layout.cell, view as ViewGroup, false)

                cell.findViewById<TextView>(R.id.cell_label).text = "  Q(${i / 2 + 1})  "

                viewHolder.addView(cell)

            } else if (i % 2 == 0) {
                val cell = LayoutInflater.from(requireContext()).inflate(R.layout.cell, view as ViewGroup, false)

                cell.findViewById<TextView>(R.id.cell_label).text = "  f(${i / 2})  "

                viewHolder.addView(cell)

            }


            for (j in 0 until matrixHolder?.mRows!!) {
                val cell = LayoutInflater.from(requireContext()).inflate(R.layout.cell, view as ViewGroup, false)

                cell.findViewById<TextView>(R.id.cell_label).text = matrix[j][i].toString()

                cell.setOnClickListener {
                    val message = if (i == 2)
                        mIteration?.calculateInitStep(1, matrix[j][0], 0f, matrix[j][1])
                    else if (i > 2 && i % 2 == 0) {
                        mIteration?.calculateStep(
                            i / 2,
                            matrix[j][0],
                            mPeriods!![i / 2 - 1],
                            DynamicProgramingLogic.caclculateQuantity(matrix[j][0], mPeriods!![i / 2 - 1])
                        )
                    } else {
                        " "
                    }

                    DialogFragmentBuilder().Builder().apply {
                        setContentText(message!!)
                    }.build(childFragmentManager)
                }

                viewHolder.addView(cell)
            }

            table.addView(viewHolder)
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(bundle: Bundle) =
            OutputFragment().apply {
                arguments = bundle
            }

        @JvmStatic
        fun newInstance(listener: MatrixGetter): OutputFragment {
            val fragment = OutputFragment()

            fragment.setMatrixGetter(listener)

            return fragment

        }
    }
}
