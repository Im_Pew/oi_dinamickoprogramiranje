package hr.dturic.faculty.dinamikoprogramiranje.matrix


class Iteration(
        orderingStep: Float,
        maxOrder: Float,
        orderingPrice: Float,
        storageCost: Float,
        matrix: DynamicProgramingLogic
) {

    val mMatrix = matrix

    val mOrderingStep = orderingStep
    val mMaxOrder = maxOrder
    val mOrderingPrice = orderingPrice
    val mStorageCost = storageCost

    init {
        for (i in 0 until mMatrix.mRows) {
            var quantity = mMatrix.getValue(i, 0)
            val stock = mMatrix.getValue(i, 1)

            var sum = 0f
            if (stock > 0) {
                sum += orderingPrice
            }
            sum += quantity * storageCost

            mMatrix.insertValue(i, 2, sum)
        }
    }

    fun calculateInitStep(period: Int, stock: Float, need: Float, order: Float): String {

        var value = "f($period)[$stock] = [$order, $stock] = "

        value += if (order > 0) {
            "$mOrderingPrice + "
        } else {
            "0 + "
        }

        value += if (stock > 0) {
            "${mStorageCost * stock}"
        } else {
            "0"
        }

        return value
    }

    fun calculateStep(period: Int, stock: Float, need: Float, orders: FloatArray): String {

        val value = StringBuilder()

        for (order in orders) {
            value.append("f($period)[$order] = [$order, $stock] + [$stock + $need - $order] = ")

            value.append(if (order > 0) {
                "$mOrderingPrice + "
            } else {
                "0 + "
            })

            value.append(if (stock > 0) {
                "${mStorageCost * stock}"
            } else {
                "0"
            })

            if (period > 1)
                value.append(" + ${mMatrix.getValue(stock + need - order, period)}             ")

            value.append("\n")
        }

        return value.toString()
    }

    fun calculateStepValue(period: Int, stock: Float, need: Float): Float {

        var currentStock = stock

        for (j in 0 until mMatrix.mRows) {

            val orders = DynamicProgramingLogic.caclculateQuantity(currentStock, need)

            var value: Float
            val min = FloatArray(orders.size)

            for ((i, v) in orders.withIndex()) {
                value = 0f

                value = if (v > 0) {
                    value + mOrderingPrice
                } else {
                    value + 0
                }

                value = if (currentStock > 0) {
                    value + (mStorageCost * currentStock)
                } else {
                    value + 0
                }

                value += mMatrix.getValue(currentStock + need - v, period)

                min[i] = value

            }
            mMatrix.insertValue(j, period * 2, min.min()!!)
            mMatrix.insertValue(j, period * 2 - 1, orders[min.indexOf(min.min()!!).toFloat().toInt()])

            currentStock += mOrderingStep
        }


        return 0f
    }
}
