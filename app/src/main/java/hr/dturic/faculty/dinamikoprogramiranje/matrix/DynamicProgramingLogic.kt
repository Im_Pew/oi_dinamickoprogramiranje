package hr.dturic.faculty.dinamikoprogramiranje.matrix

class DynamicProgramingLogic(rows: Int, columns: Int, rates: FloatArray) {

    var mRows = rows
        get() = field

    var mColumns: Int = columns
        get() = field

    val matrix: Array<FloatArray> = Array(size = rows) { FloatArray(columns) }

    init {

        for ((index) in matrix.withIndex()) {
            matrix[index][0] = rates[index]
        }
    }

    fun insertValue(row: Int, column: Int, value: Float) {

        matrix[row][column] = value
    }

    fun getValue(row: Int, column: Int): Float {

        return matrix[row][column]
    }

    fun getValue(stock: Float, period: Int): Float {
        for (i in 0 until mRows) {
            if (matrix[i][0] == stock)
                return matrix[i][((period - 1) * 2)]
        }

        return 0f
    }

    fun getValueOfQuantity(stock: Float, period: Int): Float {
        for (i in 0 until mRows) {
            if (matrix[i][0] == stock)
                return matrix[i][(period * 2) + 1]
        }

        return 0f
    }

    fun printMatrix(): String {

        var matrix = ""

        for (row in this.matrix) {
            for (value in row) {
                matrix += "$value "
            }
            matrix += "\n"
        }

        return matrix
    }

    companion object {

        var mWarehouseCap: Float = 0.0f
        var mMaxOrder: Float = 0.0f
        var mRates: Float = 0.0f

        fun caclculateQuantity(stock: Float, need: Float): FloatArray {

            val low = if (stock + need - mWarehouseCap <= 0) {
                0f
            } else {
                stock + need - mWarehouseCap
            }

            val high = if (mMaxOrder <= stock + need) {
                mMaxOrder
            } else {
                stock + need
            }

            val count = (high - low) / mRates

            val result = FloatArray(size = count.toInt() + 1)

            for (i in 0..count.toInt()) {
                result[i] = low + (mRates * i)
            }

            return result
        }
    }
}